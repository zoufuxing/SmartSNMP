#!/bin/bash

# SNMPv2 cases

snmpget -v2c -cpublic localhost .0 
snmpget -v2c -cpublic localhost .1.3
snmpget -v2c -cpublic localhost .1.4
snmpget -v2c -cpublic localhost .1.5.6.7.8.100

snmpget -v2c -cpublic localhost .1.3.6.1.2.1.1.9.1.2.1
snmpget -v2c -cpublic localhost .1.3.6.1.2.1.1.1.0 .1.3.6.1.2.1.1.2.0
snmpget -v2c -cpublic localhost .1.3.6.1.2.1.1.9.1.1
snmpget -v2c -cpublic localhost .1.3.6.1.2.1.1.9.1.2
snmpget -v2c -cpublic localhost .1.3.6.1.2.1.1.9.1.5
snmpget -v2c -cpublic localhost .1.3.6.1.2.1.1.0

snmpgetnext -v2c -cpublic localhost .0 
snmpgetnext -v2c -cpublic localhost .1.3
snmpgetnext -v2c -cpublic localhost .1.4
snmpgetnext -v2c -cpublic localhost .1.5.6.7.8.100

snmpbulkget -v2c -cpublic localhost .1.3.6.1.2.1.1

# Error test (no access)
snmpset -v2c -cpublic localhost .1.3.6.1.2.1.1.9.1.1 i 1
# Error test (no access)
snmpset -v2c -cpublic localhost .1.3.6.1.2.1.4.1.0 s "This agent is really smart!"
# Error test (no access)
snmpset -v2c -cpublic localhost .1.3.6.1.2.1.4.1.0 i 8888

# Error test (no access)
snmpset -v2c -cprivate localhost .1.3.6.1.2.1.1.9.1.1 i 1
# Error test (wrong type)
snmpset -v2c -cprivate localhost .1.3.6.1.2.1.4.1.0 s "This agent is really smart!"
# OK
snmpset -v2c -cprivate localhost .1.3.6.1.2.1.4.1.0 i 8888

snmpwalk -v2c -cpublic localhost .1.3.6.1.2.1.1
snmpwalk -v2c -cpublic localhost .1.3.6.1

# SNMPv3 cases for no authentication

snmpget -u roNoAuthUser -l noAuthNoPriv localhost .0 
snmpget -u roNoAuthUser -l noAuthNoPriv localhost .1.3
snmpget -u roNoAuthUser -l noAuthNoPriv localhost .1.4
snmpget -u roNoAuthUser -l noAuthNoPriv localhost .1.5.6.7.8.100

snmpget -u roNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.1.9.1.2.1
snmpget -u roNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.1.1.0 .1.3.6.1.2.1.1.2.0
snmpget -u roNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.1.9.1.1
snmpget -u roNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.1.9.1.2
snmpget -u roNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.1.9.1.5
snmpget -u roNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.1.0

snmpgetnext -u roNoAuthUser -l noAuthNoPriv localhost .0 
snmpgetnext -u roNoAuthUser -l noAuthNoPriv localhost .1.3
snmpgetnext -u roNoAuthUser -l noAuthNoPriv localhost .1.4
snmpgetnext -u roNoAuthUser -l noAuthNoPriv localhost .1.5.6.7.8.100

snmpbulkget -u roNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.1

# Error test (no access)
snmpset -u roNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.1.9.1.1 i 1
# Error test (no access)
snmpset -u roNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.4.1.0 s "This agent is really smart!"
# Error test (no access)
snmpset -u roNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.4.1.0 i 8888

# Error test (no access)
snmpset -u rwNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.1.9.1.1 i 1
# Error test (wrong type)
snmpset -u rwNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.4.1.0 s "This agent is really smart!"
# OK
snmpset -u rwNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.4.1.0 i 8888

snmpwalk -u roNoAuthUser -l noAuthNoPriv localhost .1.3.6.1.2.1.1
snmpwalk -u roNoAuthUser -l noAuthNoPriv localhost .1.3.6.1

# SNMPv3 cases for authentication only

snmpget -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .0 
snmpget -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.3
snmpget -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.4
snmpget -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.5.6.7.8.100

snmpget -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.3.6.1.2.1.1.9.1.2.1
snmpget -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.3.6.1.2.1.1.1.0 .1.3.6.1.2.1.1.2.0
snmpget -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.3.6.1.2.1.1.9.1.1
snmpget -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.3.6.1.2.1.1.9.1.2
snmpget -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.3.6.1.2.1.1.9.1.5
snmpget -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.3.6.1.2.1.1.0

snmpgetnext -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .0 
snmpgetnext -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.3
snmpgetnext -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.4
snmpgetnext -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.5.6.7.8.100

snmpbulkget -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.3.6.1.2.1.1

# Error test (no access)
snmpset -u rwAuthUser -a MD5 -A "rwAuthUser" -l authNoPriv localhost .1.3.6.1.2.1.1.9.1.1 i 1
# Errwr test (wrwng type)
snmpset -u rwAuthUser -a MD5 -A "rwAuthUser" -l authNoPriv localhost .1.3.6.1.2.1.4.1.0 s "This agent is really smart!"
# OK
snmpset -u rwAuthUser -a MD5 -A "rwAuthUser" -l authNoPriv localhost .1.3.6.1.2.1.4.1.0 i 8888

snmpwalk -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.3.6.1.2.1.1
snmpwalk -u roAuthUser -a MD5 -A "roAuthUser" -l authNoPriv localhost .1.3.6.1

# SNMPv3 cases for authentication and privacy

snmpget -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .0 
snmpget -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.3
snmpget -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.4
snmpget -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.5.6.7.8.100

snmpget -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.3.6.1.2.1.1.9.1.2.1
snmpget -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.3.6.1.2.1.1.1.0 .1.3.6.1.2.1.1.2.0
snmpget -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.3.6.1.2.1.1.9.1.1
snmpget -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.3.6.1.2.1.1.9.1.2
snmpget -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.3.6.1.2.1.1.9.1.5
snmpget -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.3.6.1.2.1.1.0

snmpgetnext -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .0 
snmpgetnext -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.3
snmpgetnext -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.4
snmpgetnext -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.5.6.7.8.100

snmpbulkget -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.3.6.1.2.1.1

# Errwr test (no access)
snmpset -u rwAuthPrivUser -a MD5 -A "rwAuthPrivUser" -x AES -X "rwAuthPrivUser" -l authPriv localhost .1.3.6.1.2.1.1.9.1.1 i 1
# Errwr test (wrwng type)
snmpset -u rwAuthPrivUser -a MD5 -A "rwAuthPrivUser" -x AES -X "rwAuthPrivUser" -l authPriv localhost .1.3.6.1.2.1.4.1.0 s "This agent is really smart!"
# OK
snmpset -u rwAuthPrivUser -a MD5 -A "rwAuthPrivUser" -x AES -X "rwAuthPrivUser" -l authPriv localhost .1.3.6.1.2.1.4.1.0 i 8888

snmpwalk -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.3.6.1.2.1.1
snmpwalk -u roAuthPrivUser -a MD5 -A "roAuthPrivUser" -x AES -X "roAuthPrivUser" -l authPriv localhost .1.3.6.1
